#include <iostream>
using namespace std;

/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main() {
	string kt, key, output = "";
	int i, index = 0;
	
	
	cout << "Klartext: ";
	cin >> kt;
	
	output = kt;
	
	cout << "Key: ";
	cin >> key;
	
	for(i = 1; i < kt.length(); i++){
		if(index == key.length()){
			index = 0;
		}
		output[i] = kt[i] ^ key[index];
		
		index++;
		
	}
	
	cout << output;
	
	return 0;
}
