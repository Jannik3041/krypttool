#include <iostream>
using namespace std;
/* run this program using the console pauser or add your own getch, system("pause") or input loop */

int main(int argc, char** argv) {
	
	string kt, key, output;
	int asc1, asc2, i, j, index = 0;
	
	cout << "Klartext: ";
	cin >> kt;
	
	cout << "Key: ";
	cin >> key;
	
	for(i= 0; i < kt.length(); i++){
		asc1 = kt[i];
		asc2 = key[i];
		
		asc1 = asc1 + asc2;
		
		kt[i] = asc1;
		
		cout << kt[i] << "   " << asc1 << "   " << i << endl;
	}
	
	cout << "Finished 1 " << kt << endl << endl;
	
	for(i= 0; i < kt.length(); i++){
		asc1 = kt[i];
		asc2 = key[i];
		
		asc1 = asc1 - asc2;
		
		kt[i] = asc1;
		
		cout << kt[i] << "   " << asc1 << "   " << i << endl;
	}
	
	cout << "Finished 2 " << kt << endl << endl;
	
	
	return 0;
}
