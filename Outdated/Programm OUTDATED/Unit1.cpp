//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
#include "class.h"
#include "prj_KryptTool.h"
#include <string>


//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tfrm_KryptTool *frm_KryptTool;
//---------------------------------------------------------------------------
__fastcall Tfrm_KryptTool::Tfrm_KryptTool(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_KryptTool::Hilfe1Click(TObject *Sender)
{
	//�ffnen von anderen forms
	frm_Hilfe -> Show();
	frm_KryptTool -> Hide();
}
//---------------------------------------------------------------------------

// Encoden Button
void __fastcall Tfrm_KryptTool::btn_encodenClick(TObject *Sender)
{
	// Umwandlung des von Unicodestring in Ansistring
	AnsiString str = txt_1->Text;
	string s(str.c_str());
	// L�nge des Strings
	int l = str.Length();
	Label1->Caption = l;

	//Objekt erstellen
	code code1(s,l);

	//Textbox clearen
	txt_1->Text = "";

	AnsiString str2;
	str2 = (code1.rot13().c_str());
	txt_2->Text = str2;
	}
//---------------------------------------------------------------------------

// Decoden Button
void __fastcall Tfrm_KryptTool::btn_decodenClick(TObject *Sender)
{
	// Umwandlung des von Unicodestring in Ansistring
	AnsiString str = txt_2->Text;
	string s(str.c_str());
	// L�nge des Strings
	int l = str.Length();
	Label1->Caption = l;

	//Objekt erstellen
	code code1(s,l);

	//Textbox clearen
	txt_2->Text = "";

	AnsiString str2;
	str2 = (code1.rot13().c_str());
	txt_1->Text = str2;
}
//---------------------------------------------------------------------------

