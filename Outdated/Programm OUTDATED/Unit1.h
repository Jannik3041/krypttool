//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class Tfrm_KryptTool : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TLabel *lbl_KryptTool;
	TMainMenu *menu_Main;
	TMenuItem *Datei1;
	TMenuItem *ffnen1;
	TMenuItem *Speichern1;
	TMenuItem *Drucken1;
	TMenuItem *N1;
	TMenuItem *Exit1;
	TMenuItem *Help1;
	TMenuItem *Hilfe1;
	TMenuItem *N2;
	TEdit *txt_1;
	TEdit *txt_2;
	TComboBox *cb_schluessel1;
	TButton *btn_encoden;
	TLabel *Label1;
	TComboBox *cb_schluessel2;
	TEdit *ed_key;
	TButton *btn_decoden;
	TLabel *lb_decoded;
	TLabel *lb_encoded;
	TLabel *lb_plus;
	void __fastcall Hilfe1Click(TObject *Sender);
	void __fastcall btn_encodenClick(TObject *Sender);
	void __fastcall btn_decodenClick(TObject *Sender);
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall Tfrm_KryptTool(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_KryptTool *frm_KryptTool;
//---------------------------------------------------------------------------
#endif
