//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
//---------------------------------------------------------------------------
class Tfrm_Hilfe : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall Tfrm_Hilfe(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_Hilfe *frm_Hilfe;
//---------------------------------------------------------------------------
#endif
