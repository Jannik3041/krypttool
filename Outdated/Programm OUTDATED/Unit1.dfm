object frm_KryptTool: Tfrm_KryptTool
  Left = 0
  Top = 0
  BorderStyle = bsToolWindow
  Caption = 'KryptTool'
  ClientHeight = 480
  ClientWidth = 640
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = menu_Main
  OldCreateOrder = False
  Scaled = False
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_KryptTool: TLabel
    Left = 223
    Top = 8
    Width = 136
    Height = 39
    Caption = 'KryptTool'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = []
    ParentFont = False
  end
  object Label1: TLabel
    Left = 448
    Top = 29
    Width = 31
    Height = 13
    Caption = 'Label1'
  end
  object lb_decoded: TLabel
    Left = 8
    Top = 60
    Width = 70
    Height = 19
    Caption = 'Decoded'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb_encoded: TLabel
    Left = 8
    Top = 268
    Width = 68
    Height = 19
    Caption = 'Encoded'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -16
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb_plus: TLabel
    Left = 175
    Top = 234
    Width = 17
    Height = 25
    Caption = '+'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object txt_1: TEdit
    Left = 8
    Top = 85
    Width = 624
    Height = 150
    AutoSize = False
    TabOrder = 0
  end
  object txt_2: TEdit
    Left = 8
    Top = 292
    Width = 624
    Height = 150
    AutoSize = False
    TabOrder = 2
  end
  object cb_schluessel1: TComboBox
    Left = 8
    Top = 241
    Width = 161
    Height = 21
    TabOrder = 1
    Text = 'Verschl'#252'sselung - 1'
    Items.Strings = (
      'ROT 13'
      'XOR'
      'Passwort'
      'Primzahlen')
  end
  object btn_encoden: TButton
    Left = 550
    Top = 241
    Width = 82
    Height = 25
    Caption = 'Verschl'#252'sseln'
    TabOrder = 3
    OnClick = btn_encodenClick
  end
  object cb_schluessel2: TComboBox
    Left = 198
    Top = 241
    Width = 161
    Height = 21
    TabOrder = 4
    Text = 'Verschl'#252'sselung - 2'
    Items.Strings = (
      'Keine'
      'ROT 13'
      'XOR'
      'Passwort'
      'Primzahlen')
  end
  object ed_key: TEdit
    Left = 365
    Top = 241
    Width = 41
    Height = 21
    TabOrder = 5
  end
  object btn_decoden: TButton
    Left = 550
    Top = 447
    Width = 82
    Height = 25
    Caption = 'Entschl'#252'sseln'
    TabOrder = 6
    OnClick = btn_decodenClick
  end
  object menu_Main: TMainMenu
    Top = 448
    object Datei1: TMenuItem
      Caption = 'Datei'
      object ffnen1: TMenuItem
        Caption = #214'ffnen'
      end
      object Speichern1: TMenuItem
        Caption = 'Speichern'
      end
      object Drucken1: TMenuItem
        Caption = 'Drucken'
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Exit1: TMenuItem
        Caption = 'Exit'
      end
    end
    object Help1: TMenuItem
      Caption = 'Mehr'
      object Hilfe1: TMenuItem
        Caption = 'Hilfe'
        OnClick = Hilfe1Click
      end
      object N2: TMenuItem
        Caption = 'Credits'
      end
    end
  end
end
