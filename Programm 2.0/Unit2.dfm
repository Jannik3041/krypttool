object frm_hilfe: Tfrm_hilfe
  Left = 0
  Top = 0
  Caption = 'Hilfe'
  ClientHeight = 101
  ClientWidth = 719
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lb_hilfe: TLabel
    Left = 8
    Top = 39
    Width = 696
    Height = 13
    Caption = 
      'W'#228'hlen Sie eine Verschl'#252'sselung aus, Geben Sie einen Klartext ei' +
      'n und  geben Sie einen Key ein. Danach dr'#252'cken Sie auf Encoden o' +
      'der Decoden.'
  end
  object lb_help: TLabel
    Left = 8
    Top = 8
    Width = 48
    Height = 25
    Caption = 'Hilfe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object lb_hilfe2: TLabel
    Left = 8
    Top = 58
    Width = 245
    Height = 13
    Caption = 'Der Schl'#252'ssel muss mindestens 3 Zeichen lang sein!'
  end
  object MainMenu1: TMainMenu
    Left = 776
    object Datei1: TMenuItem
      Caption = '&Datei'
      object Beenden1: TMenuItem
        Caption = '&Beenden'
        OnClick = Beenden1Click
      end
    end
  end
end
