//---------------------------------------------------------------------------

#ifndef Unit3H
#define Unit3H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class Tfrm_credits : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TMainMenu *MainMenu1;
	TMenuItem *Datei1;
	TMenuItem *Beenden1;
	TLabel *lb_entwickler;
	TLabel *lb_jannik;
	TLabel *lb_daniel;
	TLabel *lb_tom;
	void __fastcall Beenden1Click(TObject *Sender);
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall Tfrm_credits(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_credits *frm_credits;
//---------------------------------------------------------------------------
#endif
