object frm_Krypto: Tfrm_Krypto
  Left = 0
  Top = 0
  Caption = 'Krypto Verschl'#252'sselungen'
  ClientHeight = 367
  ClientWidth = 797
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = mm_menu
  OldCreateOrder = False
  OnCreate = FormCreate
  PixelsPerInch = 96
  TextHeight = 13
  object lbl_eingabe: TLabel
    Left = 16
    Top = 16
    Width = 128
    Height = 39
    Caption = 'Eingabe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lbl_ausgabe: TLabel
    Left = 536
    Top = 16
    Width = 136
    Height = 39
    Caption = 'Ausgabe'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -32
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb_op1: TLabel
    Left = 288
    Top = 96
    Width = 47
    Height = 13
    Caption = 'Option 1'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb_op2: TLabel
    Left = 391
    Top = 96
    Width = 47
    Height = 13
    Caption = 'Option 2'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object lb_key: TLabel
    Left = 288
    Top = 42
    Width = 24
    Height = 16
    Caption = 'Key'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -13
    Font.Name = 'Tahoma'
    Font.Style = [fsBold]
    ParentFont = False
  end
  object redt_eingabe: TRichEdit
    Left = 16
    Top = 61
    Width = 249
    Height = 252
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'redt_eingabe')
    ParentFont = False
    TabOrder = 0
    Zoom = 100
  end
  object redt_ausgabe: TRichEdit
    Left = 536
    Top = 61
    Width = 249
    Height = 252
    Font.Charset = ANSI_CHARSET
    Font.Color = clWindowText
    Font.Height = -11
    Font.Name = 'Tahoma'
    Font.Style = []
    Lines.Strings = (
      'redt_ausgabe')
    ParentFont = False
    TabOrder = 8
    Zoom = 100
  end
  object btn_encode: TButton
    Left = 288
    Top = 272
    Width = 73
    Height = 41
    Caption = 'Encode'
    TabOrder = 7
    OnClick = btn_encodeClick
  end
  object cb_rot13: TCheckBox
    Left = 288
    Top = 115
    Width = 97
    Height = 17
    Caption = 'ROT 13'
    TabOrder = 2
  end
  object cb_xor: TCheckBox
    Left = 288
    Top = 138
    Width = 97
    Height = 17
    Caption = 'XOR'
    TabOrder = 3
  end
  object cb_viginere: TCheckBox
    Left = 288
    Top = 161
    Width = 97
    Height = 17
    Caption = 'Vigin'#233're'
    TabOrder = 4
  end
  object cb_primzahlen: TCheckBox
    Left = 288
    Top = 184
    Width = 97
    Height = 17
    Caption = 'Primzahlen'
    Enabled = False
    TabOrder = 5
  end
  object cb_enigma: TCheckBox
    Left = 288
    Top = 207
    Width = 97
    Height = 17
    Caption = 'Enigma'
    Enabled = False
    TabOrder = 6
  end
  object edt_schluesselwort: TEdit
    Left = 288
    Top = 61
    Width = 225
    Height = 21
    TabOrder = 1
  end
  object btn_decode: TButton
    Left = 376
    Top = 272
    Width = 73
    Height = 41
    Caption = 'Decode'
    TabOrder = 9
    OnClick = btn_decodeClick
  end
  object cb_rot132: TCheckBox
    Left = 391
    Top = 115
    Width = 97
    Height = 17
    Caption = 'ROT 13'
    Enabled = False
    TabOrder = 10
  end
  object cb_xor2: TCheckBox
    Left = 391
    Top = 138
    Width = 97
    Height = 17
    Caption = 'XOR'
    Enabled = False
    TabOrder = 11
  end
  object cb_viginere2: TCheckBox
    Left = 391
    Top = 161
    Width = 97
    Height = 17
    Caption = 'Vigin'#233're'
    Enabled = False
    TabOrder = 12
  end
  object cb_primzahlen2: TCheckBox
    Left = 391
    Top = 184
    Width = 97
    Height = 17
    Caption = 'Primzahlen'
    Enabled = False
    TabOrder = 13
  end
  object cb_enigma2: TCheckBox
    Left = 391
    Top = 207
    Width = 97
    Height = 17
    Caption = 'Enigma'
    Enabled = False
    TabOrder = 14
  end
  object mm_menu: TMainMenu
    Left = 768
    object Datei1: TMenuItem
      Caption = '&Datei'
      object ffnen1: TMenuItem
        Caption = '&'#214'ffnen'
        OnClick = ffnen1Click
      end
      object N1: TMenuItem
        Caption = '-'
      end
      object Speichern1: TMenuItem
        Caption = '&Speichern'
        OnClick = Speichern1Click
      end
      object Speichernunter1: TMenuItem
        Caption = '-'
      end
      object N2: TMenuItem
        Caption = '&Drucker einrichten'
        OnClick = N2Click
      end
      object Drucken1: TMenuItem
        Caption = '&Drucken'
        OnClick = Drucken1Click
      end
      object N3: TMenuItem
        Caption = '-'
      end
      object Beenden1: TMenuItem
        Caption = '&Beenden'
        OnClick = Beenden1Click
      end
    end
    object Hilfe1: TMenuItem
      Caption = '&Hilfe'
      object ffenen1: TMenuItem
        Caption = '&'#214'ffenen'
        OnClick = ffenen1Click
      end
    end
    object Credits1: TMenuItem
      Caption = '&Credits'
      object ffnen2: TMenuItem
        Caption = '&'#214'ffnen'
        OnClick = ffnen2Click
      end
    end
  end
  object od_dateien_oeffnen: TOpenDialog
    Left = 736
  end
  object sd_abspeichern: TSaveDialog
    Left = 704
  end
  object pd_drucken: TPrintDialog
    Left = 640
  end
  object psd_drucker_einstellen: TPrinterSetupDialog
    Left = 672
  end
end
