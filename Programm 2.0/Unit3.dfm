object frm_credits: Tfrm_credits
  Left = 0
  Top = 0
  Caption = 'Credits'
  ClientHeight = 140
  ClientWidth = 209
  Color = clBtnFace
  Font.Charset = DEFAULT_CHARSET
  Font.Color = clWindowText
  Font.Height = -11
  Font.Name = 'Tahoma'
  Font.Style = []
  Menu = MainMenu1
  OldCreateOrder = False
  PixelsPerInch = 96
  TextHeight = 13
  object lb_entwickler: TLabel
    Left = 8
    Top = 8
    Width = 111
    Height = 25
    Caption = 'Entwickler'
    Font.Charset = DEFAULT_CHARSET
    Font.Color = clWindowText
    Font.Height = -21
    Font.Name = 'Tahoma'
    Font.Style = [fsBold, fsUnderline]
    ParentFont = False
  end
  object lb_jannik: TLabel
    Left = 8
    Top = 39
    Width = 64
    Height = 13
    Caption = 'Jannik R'#246'hrig'
  end
  object lb_daniel: TLabel
    Left = 8
    Top = 58
    Width = 67
    Height = 13
    Caption = 'Daniel Weitzel'
  end
  object lb_tom: TLabel
    Left = 8
    Top = 77
    Width = 63
    Height = 13
    Caption = 'Tom Weidner'
  end
  object MainMenu1: TMainMenu
    Left = 392
    object Datei1: TMenuItem
      Caption = '&Datei'
      object Beenden1: TMenuItem
        Caption = '&Beenden'
        OnClick = Beenden1Click
      end
    end
  end
end
