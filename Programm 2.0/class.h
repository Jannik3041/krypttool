#include <vcl.h>
using namespace std;

class code{
	private:
		String encoded; //encoded ist die Variable f�r den verschl�sselten Text
		String decoded; //decoded ist die Variable f�r den entschl�sselten Text
		String key;
		char ckey;
		int laenge;
		int verschiebung;

	public:
		//Variablen f�r die verschiedenen Methoden zum ent- und verschl�sseln
		//String ist der R�ckgabewert
		String rot13 (void);
		String rot13de (void);
		String xor (void);
//		String enigma (void);
		String primzahlen (void);
		String primzahlende (void);
		String viginere (void);
		String viginerede (void);

		// Konstruktor / DeKonstruktor
		code(String, int, String);
		~code();
};

// Konstruktor
code::code(String dc, int lng, String ky){
	decoded = dc;
	laenge = lng;
	key = ky;
}

// DeKonstruktor
code::~code(){
}

//---------------------------------------------------------------------------

// Method rot13 (encoding)
String code::rot13 (void){
	verschiebung = 13;
	int ascii = 0;
	int i;

	encoded = decoded;

	for(i = 1; i <= encoded.Length(); i++){
		ascii = encoded[i];
		encoded[i] = verschiebung + ascii;
	}

	return encoded;
}

//---------------------------------------------------------------------------

// Method rot13 (decoding)
String code::rot13de (void){
	verschiebung = 13;
	int ascii = 0;
	int i;

	encoded = decoded;

	for(i = 1; i <= encoded.Length(); i++){
		ascii = encoded[i];
		encoded[i] = verschiebung - ascii;
	}

	return encoded;
}

//---------------------------------------------------------------------------

String code::xor(void){
	String output = "";
	int i, index = 0;
	output = decoded;

	for(i = 1; i < laenge; i++){
		if(index == key.Length()){
			index = 0;
		}
		output[i] = decoded[i] ^ key[i];
		index++;
	}

	return output;
}

//---------------------------------------------------------------------------

 String code::primzahlen(void)
 {
	/*
	int ascii = 0, Anzahl, Start;
	int *pointer = new int[Anzahl];

	do // Create amount of primes
	{
		for (int i = Start; i >= 0 ; i++)// count from 1 - X
		{
			bool Prime = true;        // proof if prime ore not

			for (int j = 2; j < i; j++) // divide  i/x
			{
				if (i%j == 0)
				{
					Prime = false; // when rest then break
					break;
				}
			}

			if (Prime)
			{
				pointer[ascii] = i; // fill the array
				ascii++;            // ascii + 1
			}
			if (ascii > Anzahl) // leave for
			{
				break;
			}
		}
		if (ascii > Anzahl)
		{
			break;
		}
	} while (ascii > Anzahl);

	int b=0;

	for(int a = 1; a <= redt_eingabe.Length() ; a++)// to Krypt
	{

		redt_eingabe[a] = char(int((redt_eingabe[a]) + int(pointer[b])));
		b++;
		if(b == Anzahl) // when b >= Anzahl
		{
			b = 1;                 // b = 1
		}
		else
		{
		}
	}

	delete pointer;
	*/
	return 0;
 }
//----------------------------------------------------------------------------
 String code::primzahlende(void)
 {
	/*
	int ascii = 0, Anzahl, Start;
	int *pointer = new int[Anzahl];

	do // Create amount of primes
	{
		for (int i = Start; i >= 0 ; i++)// count from 1 - X
		{
			bool Prime = true;        // proof if prime ore not

			for (int j = 2; j < i; j++) // divide  i/x
			{
				if (i%j == 0)
				{
					Prime = false; // when rest then break
					break;
				}
			}

			if (Prime)
			{
				pointer[ascii] = i; // fill the array
				ascii++;            // ascii + 1
			}
			if (ascii > Anzahl) // leave for
			{
				break;
			}
		}
		if (ascii > Anzahl)
		{
			break;
		}
	} while (ascii > Anzahl);

	int b=0;

	for(int a = 1; a <= redt_eingabe.Length() ; a++)// to Krypt
	{

		redt_eingabe[a] = char(int((redt_eingabe[a]) - int(pointer[b])));
		b++;
		if(b == Anzahl) // when b >= Anzahl
		{
			b = 1;                 // b = 1
		}
		else
		{
		}
	}

	delete pointer;
	*/
	return 0;
 }

//---------------------------------------------------------------------------
String code::viginere (void){
	String output;
	int asc1 = 0, asc2 = 0, i = 0, j = 0;

	for(i= 1; i < decoded.Length(); i++){
		asc1 = decoded[i];
		asc2 = key[i];
		asc1 = asc1 + asc2;
		decoded[i] = asc1;
	}

	output = decoded;
	return output;
}

String code::viginerede (void){
	String output;
	int asc1, asc2, i, j;

	for(i= 1; i < decoded.Length(); i++){
		asc1 = decoded[i];
		asc2 = key[i];
		asc1 = asc1 - asc2;
		decoded[i] = asc1;
	}

	output = decoded;
	return output;
}
