//---------------------------------------------------------------------------

#include <vcl.h>
#pragma hdrstop

#include "Unit1.h"
#include "Unit2.h"
#include "Unit3.h"
#include "Unit4.h"
#include "class.h"
//---------------------------------------------------------------------------
#pragma package(smart_init)
#pragma resource "*.dfm"
Tfrm_Krypto *frm_Krypto;
//---------------------------------------------------------------------------
__fastcall Tfrm_Krypto::Tfrm_Krypto(TComponent* Owner)
	: TForm(Owner)
{
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::FormCreate(TObject *Sender)
{
	redt_ausgabe->Clear();                                 // Textfelt leeren
	redt_eingabe->Clear();                                 // Textfelt leeren
	edt_schluesselwort->Clear();                           // Textfelt leeren
	edt_schluesselwort->TextHint = "Schl�sselwort";        // Textfelt Hinweis einf�gen
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::Beenden1Click(TObject *Sender)
{
	frm_Krypto->Close();                                   // Programm schlie�en
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::ffnen1Click(TObject *Sender)
{
	od_dateien_oeffnen->Execute();                         // Datei �ffnen
	redt_eingabe->Lines->LoadFromFile(od_dateien_oeffnen->FileName);
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::Speichern1Click(TObject *Sender)
{
	sd_abspeichern->Execute();                             // Datei Speichern
	redt_ausgabe->Lines->SaveToFile(sd_abspeichern->FileName);
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::N2Click(TObject *Sender)
{
	psd_drucker_einstellen->Execute();                     // Drucker Eintsellen
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::Drucken1Click(TObject *Sender)
{
	int Zeile = 1;
	Printer()->BeginDoc();                                 // Drucken
	for(Zeile = 1; Zeile <= redt_ausgabe->Lines->Count; Zeile++)
	{
		Printer()->Canvas->TextOutW(10, 100*Zeile, redt_ausgabe->Lines->Strings[Zeile-1]);
	}
	Printer()->EndDoc();
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::ffenen1Click(TObject *Sender)
{
	frm_hilfe->ShowModal();                                // Hilfe �ffnen
}
//---------------------------------------------------------------------------
void __fastcall Tfrm_Krypto::ffnen2Click(TObject *Sender)
{
	frm_credits->Show();                                   // Credits �ffnen
}
//---------------------------------------------------------------------------
/*
//[CheckBox]
//---------------------------------------------------------------------------
void__fastcall Tfrm_Krypto::cb_rot13Click    (TObject *Sender){ToggelOption(Sender, cb_rot13, men_rot13); }
void__fastcall Tfrm_Krypto::cb_xorClick    (TObject *Sender){ToggelOption(Sender, cb_xor, men_xor); }
void__fastcall Tfrm_Krypto::cb_viginereClick    (TObject *Sender){ToggelOption(Sender, cb_viginere, men_viginere); }
void__fastcall Tfrm_Krypto::cb_primzahlenClick    (TObject *Sender){ToggelOption(Sender, cb_primzahlen, men_primzahlen); }
void__fastcall Tfrm_Krypto::cb_enigmaClick    (TObject *Sender){ToggelOption(Sender, cb_enigma, men_enigma); }
void__fastcall Tfrm_Krypto::cb_alleClick    (TObject *Sender){ToggelOption(Sender, cb_alle, men_alle); }
//---------------------------------------------------------------------------

//[Menu]
//---------------------------------------------------------------------------
void__fastcall Tfrm_Krypto::men_rot13Click    (TObject *Sender){ToggelOption(Sender, cb_rot13, men_rot13); }
void__fastcall Tfrm_Krypto::men_xorClick    (TObject *Sender){ToggelOption(Sender, cb_xor, men_xor); }
void__fastcall Tfrm_Krypto::men_viginereClick    (TObject *Sender){ToggelOption(Sender, cb_viginere, men_viginere); }
void__fastcall Tfrm_Krypto::men_primzahlenClick    (TObject *Sender){ToggelOption(Sender, cb_primzahlen, men_primzahlen); }
void__fastcall Tfrm_Krypto::men_enigmaClick    (TObject *Sender){ToggelOption(Sender, cb_enigma, men_enigma); }
void__fastcall Tfrm_Krypto::men_alleClick    (TObject *Sender){ToggelOption(Sender, cb_alle, men_alle); }
//---------------------------------------------------------------------------
*/

//Men�f�hrung + Encoden
void __fastcall Tfrm_Krypto::btn_encodeClick(TObject *Sender)   //Encoden Button-Event
{
	String decoded = redt_eingabe->Text; // Einlesen des Klartextes
	String decoded2 = redt_eingabe->Text; // Hilfsvariable
	String key = edt_schluesselwort->Text; // Einlesen des Keys
	String output;
	int laenge = decoded.Length(); // L�nge des Klartextes
	/* -> Nicht lauff�hig
	if(cb_rot13->Checked == true && cb_xor2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.rot13();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.xor();
	}

	if(cb_rot13->Checked == true && cb_viginere2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.rot13();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.viginere();
	}

	if(cb_xor->Checked == true && cb_rot132->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.xor();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.rot13();	
	}

	if(cb_xor->Checked == true && cb_viginere2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.xor();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.viginere();	
	}

	if(cb_viginere->Checked == true && cb_rot132->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.viginere();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.rot13();	
	}

	if(cb_viginere->Checked == true && cb_xor2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.viginere();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.xor();	
	}
	*/
	//---//
	
	if(cb_rot13->Checked == true){
		code code1(decoded, laenge, key); // Objekt erstellen und Var. �bergeben
		output = code1.rot13();	// rot13 Methode ausf�hren
	}

	if(cb_xor->Checked == true){
		code code1(decoded, laenge, key);
		output = code1.xor();
	}

	if(cb_viginere->Checked == true){
		code code1(decoded, laenge, key);
		output = code1.viginere();
	}
	
	redt_ausgabe->Text = output; // Ausgabe des Ergebnisses
}

//--------------------------------------------------------------//

//Men�f�hrung + Decoden
void __fastcall Tfrm_Krypto::btn_decodeClick(TObject *Sender)
{
String decoded = redt_eingabe->Text;
	String decoded2 = redt_eingabe->Text;
	String key = edt_schluesselwort->Text;
	String output;
	int laenge = decoded.Length();
	/*
	if(cb_rot13->Checked == true && cb_xor2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.xor();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.rot13de();
	}

	if(cb_rot13->Checked == true && cb_viginere2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.viginerede();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.rot13de();
	}

	if(cb_xor->Checked == true && cb_rot132->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.rot13de();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.xor();	
	}

	if(cb_xor->Checked == true && cb_viginere2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.viginerede();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.xor();	
	}

	if(cb_viginere->Checked == true && cb_rot132->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.rot13de();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.viginerede();	
	}

	if(cb_viginere->Checked == true && cb_xor2->Checked == true){
		code code1(decoded, laenge, key);
		decoded2 = code1.xor();

		int laenge2 = decoded2.Length();
		code code2(decoded2, laenge2, key);
		output = code2.viginerede();	
	}
	*/
	//---//
	
	if(cb_rot13->Checked == true){
		code code1(decoded, laenge, key);
		output = code1.rot13de();
	}

	if(cb_xor->Checked == true){
		code code1(decoded, laenge, key);
		output = code1.xor();
	}

	if(cb_viginere->Checked == true){
		code code1(decoded, laenge, key);
		output = code1.viginerede();
	}

	redt_ausgabe->Text = output;	
}
//---------------------------------------------------------------------------

