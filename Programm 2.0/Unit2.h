//---------------------------------------------------------------------------

#ifndef Unit2H
#define Unit2H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.Menus.hpp>
#include <Vcl.ExtCtrls.hpp>
//---------------------------------------------------------------------------
class Tfrm_hilfe : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TMainMenu *MainMenu1;
	TMenuItem *Datei1;
	TMenuItem *Beenden1;
	TLabel *lb_hilfe;
	TLabel *lb_help;
	TLabel *lb_hilfe2;
	void __fastcall Beenden1Click(TObject *Sender);
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall Tfrm_hilfe(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_hilfe *frm_hilfe;
//---------------------------------------------------------------------------
#endif
