//---------------------------------------------------------------------------

#ifndef Unit1H
#define Unit1H
//---------------------------------------------------------------------------
#include <System.Classes.hpp>
#include <Vcl.Controls.hpp>
#include <Vcl.StdCtrls.hpp>
#include <Vcl.Forms.hpp>
#include <Vcl.ComCtrls.hpp>
#include <Vcl.Dialogs.hpp>
#include <Vcl.ExtDlgs.hpp>
#include <Vcl.Menus.hpp>
//---------------------------------------------------------------------------
class Tfrm_Krypto : public TForm
{
__published:	// Von der IDE verwaltete Komponenten
	TRichEdit *redt_eingabe;
	TRichEdit *redt_ausgabe;
	TButton *btn_encode;
	TCheckBox *cb_rot13;
	TCheckBox *cb_xor;
	TCheckBox *cb_viginere;
	TCheckBox *cb_primzahlen;
	TCheckBox *cb_enigma;
	TLabel *lbl_eingabe;
	TLabel *lbl_ausgabe;
	TMainMenu *mm_menu;
	TMenuItem *Datei1;
	TMenuItem *ffnen1;
	TMenuItem *N1;
	TMenuItem *Speichern1;
	TMenuItem *Speichernunter1;
	TMenuItem *N2;
	TMenuItem *Drucken1;
	TMenuItem *N3;
	TMenuItem *Beenden1;
	TMenuItem *Hilfe1;
	TMenuItem *Credits1;
	TOpenDialog *od_dateien_oeffnen;
	TSaveDialog *sd_abspeichern;
	TPrintDialog *pd_drucken;
	TPrinterSetupDialog *psd_drucker_einstellen;
	TEdit *edt_schluesselwort;
	TMenuItem *ffenen1;
	TMenuItem *ffnen2;
	TButton *btn_decode;
	TCheckBox *cb_rot132;
	TCheckBox *cb_xor2;
	TCheckBox *cb_viginere2;
	TCheckBox *cb_primzahlen2;
	TCheckBox *cb_enigma2;
	TLabel *lb_op1;
	TLabel *lb_op2;
	TLabel *lb_key;
	void __fastcall FormCreate(TObject *Sender);
	void __fastcall Beenden1Click(TObject *Sender);
	void __fastcall ffnen1Click(TObject *Sender);
	void __fastcall Speichern1Click(TObject *Sender);
	void __fastcall N2Click(TObject *Sender);
	void __fastcall Drucken1Click(TObject *Sender);
	void __fastcall ffenen1Click(TObject *Sender);
	void __fastcall ffnen2Click(TObject *Sender);
	void __fastcall btn_encodeClick(TObject *Sender);
	void __fastcall btn_decodeClick(TObject *Sender);
private:	// Benutzer-Deklarationen
public:		// Benutzer-Deklarationen
	__fastcall Tfrm_Krypto(TComponent* Owner);
};
//---------------------------------------------------------------------------
extern PACKAGE Tfrm_Krypto *frm_Krypto;
//---------------------------------------------------------------------------
#endif
